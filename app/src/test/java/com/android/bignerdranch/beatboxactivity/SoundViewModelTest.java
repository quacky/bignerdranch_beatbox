package com.android.bignerdranch.beatboxactivity;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SoundViewModelTest {

    private BeatBox mBeatBox;
    private Sound mSound;
    private SoundViewModel mSubject;
    @Before
    public void setUp() throws Exception {
        mBeatBox = mock(BeatBox.class);
        mSound = new Sound("assetPath");
        mSubject = new SoundViewModel(mBeatBox);
        mSubject.setmSound(mSound);
    }

    @Test
    public void exposeSoundNameAsTitle() {
        assertThat(mSubject.getSoundTitle(), is(mSound.getmName()));

    }

    @Test
    public void callsBeatBoxPlayOnButtonClicked() {
        mSubject.onButtonClicked();
        // check that method play of BeatBox is called with mSound as parameter
        verify(mBeatBox).play(mSound);


    }
}