package com.android.bignerdranch.beatboxactivity;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class SoundViewModel extends BaseObservable {
    private Sound mSound;
    private BeatBox mBeatBox;

    public SoundViewModel(BeatBox BeatBox) {
        mBeatBox = BeatBox;
    }

    public void setmSound(Sound mSound) {
        this.mSound = mSound;
        //inform layout file that sound was updated.
        notifyChange();
    }

    public Sound getmSound() {
        return mSound;
    }

    @Bindable
    public String getSoundTitle() {
        return mSound.getmName();
    }

    public void onButtonClicked() {
        mBeatBox.play(mSound);
    }
}
